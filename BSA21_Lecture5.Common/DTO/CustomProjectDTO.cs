﻿namespace BSA21_Lecture5.Common.DTO
{
    public class CustomProjectDTO
    {
        public ProjectDTO Project {get; set;}
        public TaskDTO LongestDescriptionTask {get; set;}
        public TaskDTO ShortestNameTask {get; set;}
        public int TotalCountOfTeamUsers {get; set;}
    }
}
