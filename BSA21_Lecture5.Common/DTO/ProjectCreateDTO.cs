﻿using System.Text.Json.Serialization;

namespace BSA21_Lecture5.Common.DTO
{
    public class ProjectCreateDTO : ProjectDTO
    {
         [JsonIgnore] public override int Id { get => base.Id; set => base.Id = value; }
    }
}
