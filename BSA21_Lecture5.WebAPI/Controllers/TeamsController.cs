﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BSA21_Lecture5.BLL.Interfaces;
using BSA21_Lecture5.Common.DTO;
using BSA21_Lecture5.DAL.Entities;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Task = System.Threading.Tasks.Task;

namespace BSA21_Lecture5.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TeamsController : ControllerBase
    {
        readonly IService<Team, TeamDTO> teamService;
        readonly IService<User, UserDTO> userService;
        readonly IValidator<TeamCreateDTO> validator;

        public TeamsController(IService<Team, TeamDTO> teamService, IService<User, UserDTO> userService, IValidator<TeamCreateDTO> validator)
        {
            this.teamService = teamService;
            this.userService = userService;
            this.validator = validator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeamDTO>>> Get()
        {
            return Ok(await Task.Run(() => teamService.Get()));
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<TeamDTO>> Get(int id)
        {
            var item = await Task.Run(() => teamService.Get(id));
            return item == null ? NotFound($"There is no such team with Id = {id}") : Ok(item);
        }

        [HttpPost]
        public async Task<ActionResult<TeamDTO>> Post([FromBody] TeamCreateDTO dto)
        {
            var validationResult = await validator.ValidateAsync(dto);
            if (!validationResult.IsValid) return BadRequest(validationResult.Errors.First().ToString());
            var team = await Task.Run(() => teamService.Create(dto));
            return CreatedAtAction("POST", team);
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<TeamDTO>> Put(int id, [FromBody] TeamCreateDTO dto)
        {
            var validationResult = await validator.ValidateAsync(dto);
            if (!validationResult.IsValid) return BadRequest(validationResult.Errors.First().ToString());
            dto.Id = id;
            var updated = await Task.Run(() => teamService.Update(dto));
            return updated ? Ok(await Task.Run(() => teamService.Get(id))) : NotFound($"No team with ID {id}");
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            var deleted = await Task.Run(() => teamService.Delete(id));
            return deleted ? NoContent() : NotFound($"No team with ID {id}");
        }

        [HttpGet("withUsers/{minAge:int}/{maxAge:int}")]
        public async Task<ActionResult<IEnumerable<KeyValuePair<TeamDTO,IEnumerable<UserDTO>>>>> GetTeamsWithUsers(int minAge = 10, int maxAge = 80)
        {
            var query = from team in teamService.Get()
                        orderby team.CreatedAt descending
                        select KeyValuePair.Create(team,
                            from user in userService.Get(u => u.TeamId == team.Id &&
                                                         DateTime.Now.Year - u.BirthDay.Year >= minAge &&
                                                         DateTime.Now.Year - u.BirthDay.Year <= maxAge)
                            orderby user.RegisteredAt descending
                            select user);
            return Ok(await Task.Run(query.AsEnumerable));
        }

        [HttpGet("andUsers")]
        public async Task<ActionResult<IEnumerable<KeyValuePair<TeamDTO, IEnumerable<UserDTO>>>>> GetTeamsWithUsers()
        {
            var query = from t in teamService.Get()
                        select KeyValuePair.Create(t, from u in userService.Get(ue => ue.TeamId == t.Id) select u);
            return Ok(await Task.Run(query.AsEnumerable));
        }
    }
}
