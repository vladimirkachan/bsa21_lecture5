﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace BSA21_Lecture5.BLL.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null);
        TEntity Get(int id);
        TEntity Create(TEntity entity);
        bool Update(TEntity entity);
        bool Delete(int id);
    }
}
