﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace BSA21_Lecture5.BLL.Interfaces
{
    public interface IService<TEntity, TDto>
    {
        IEnumerable<TDto> Get(Expression<Func<TEntity, bool>> filter = null);
        TDto Get(int id);
        TDto Create(TDto dto);
        bool Update(TDto dto);
        bool Delete(int id);
        TEntity GetEntity(int id);
    }
}
