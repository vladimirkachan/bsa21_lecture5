﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using BSA21_Lecture5.DAL;
using BSA21_Lecture5.DAL.Context;
using BSA21_Lecture5.DAL.Entities;

namespace BSA21_Lecture5.BLL.Repositories
{
    public sealed class UserRepository : BaseRepository<User>
    {
        public UserRepository(DataContext context) : base(context) {}

        public override IEnumerable<User> Get(Expression<Func<User, bool>> filter = null)
        {
            if (filter == null) return db.Users.AsEnumerable();
            var f = filter.Compile();
            var query = from i in db.Users.AsEnumerable()
                        where f(i)
                        select i;
            return query.AsEnumerable();
        }
        public override User Get(int id)
        {
            return db.Users.Find(id);
        }
        public override User Create(User entity)
        {
            var value = db.Users.Add(entity).Entity;
            db.SaveChanges();
            return value;
        }
        public override bool Update(User entity)
        {
            var item = Get(entity.Id);
            if (item == null) return false;
            db.Users.Remove(item);
            db.Users.Add(item);
            return true;
        }
        public override bool Delete(int id)
        {
            var item = Get(id);
            if (item == null) return false;
            db.Users.Remove(item);
            db.SaveChanges();
            return true;
        }

    }
}
