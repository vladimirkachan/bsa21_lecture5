﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using BSA21_Lecture5.BLL.Interfaces;
using BSA21_Lecture5.DAL;
using BSA21_Lecture5.DAL.Context;

namespace BSA21_Lecture5.BLL.Repositories
{
    public abstract class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly DataContext db;

        protected BaseRepository(DataContext context)
        {
            db = context;
        }
        public abstract IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null);
        public abstract TEntity Get(int id);
        public abstract TEntity Create(TEntity entity);
        public abstract bool Update(TEntity entity);
        public abstract bool Delete(int id);
    }
}
