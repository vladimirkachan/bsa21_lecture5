﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using BSA21_Lecture5.DAL;
using BSA21_Lecture5.DAL.Context;
using BSA21_Lecture5.DAL.Entities;

namespace BSA21_Lecture5.BLL.Repositories
{
    public sealed class TeamRepository : BaseRepository<Team>
    {
        public TeamRepository(DataContext context) : base(context) {}

        public override IEnumerable<Team> Get(Expression<Func<Team, bool>> filter = null)
        {
            if (filter == null) return db.Teams.AsEnumerable();
            var f = filter.Compile();
            var query = from i in db.Teams.AsEnumerable()
                        where f(i)
                        select i;
            return query.AsEnumerable();
        }
        public override Team Get(int id)
        {
            return db.Teams.Find(id);
        }
        public override Team Create(Team entity)
        {
            var value = db.Teams.Add(entity).Entity; 
            db.SaveChanges();
            return value;
        }
        public override bool Update(Team entity)
        {
            var item = Get(entity.Id);
            if (item == null) return false;
            db.Teams.Remove(item);
            db.Teams.Add(entity);
            return true;
        }
        public override bool Delete(int id)
        {
            var item = Get(id);
            if (item == null) return false;
            db.Teams.Remove(item);
            db.SaveChanges();
            return true;
        }

    }
}
