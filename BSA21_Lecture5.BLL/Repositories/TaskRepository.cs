﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using BSA21_Lecture5.DAL;
using BSA21_Lecture5.DAL.Context;
using BSA21_Lecture5.DAL.Entities;

namespace BSA21_Lecture5.BLL.Repositories
{
    public sealed class TaskRepository : BaseRepository<Task>
    {
        public TaskRepository(DataContext context) : base(context) {}

        public override IEnumerable<Task> Get(Expression<Func<Task, bool>> filter = null)
        {
            if (filter == null) return db.Tasks.AsEnumerable();
            var f = filter.Compile();
            var query = from i in db.Tasks.AsEnumerable()
                        where f(i)
                        select i;
            return query.AsEnumerable();
        }
        public override Task Get(int id)
        {
            return db.Tasks.Find(id);
        }
        public override Task Create(Task entity)
        {
            var value = db.Tasks.Add(entity).Entity;
            db.SaveChanges();
            return value;
        }
        public override bool Update(Task entity)
        {
            var item = Get(entity.Id);
            if (item == null) return false;
            item.ProjectId = entity.ProjectId;
            db.Tasks.Remove(item);
            db.Tasks.Add(entity);
            db.SaveChanges();
            return true;
        }
        public override bool Delete(int id)
        {
            var item = Get(id);
            if (item == null) return false;
            db.Tasks.Remove(item);
            db.SaveChanges();
            return true;
        }

    }
}
